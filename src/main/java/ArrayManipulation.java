import java.util.Arrays;
import java.util.Scanner;

public class ArrayManipulation {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int[] numbers = { 5, 8, 3, 2, 7 };
        String[] names = { "Alice", "Bob", "Charlie", "David" };
        double[] valus = new double[4];
        String[] reversedNames = { "-", "-", "-", "-", };

        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i] + " ");
        }
        System.out.println("");

        for (String i : names) {
            System.out.print(i + " ");
        }
        System.out.println("");

        for (int i = 0; i < valus.length; i++) {
            valus[i] = kb.nextDouble();
        }

        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum = sum + numbers[i];
        }
        System.out.println(sum);

        double max = Arrays.stream(valus).max().getAsDouble();
        System.out.println(max);

        int j = 3;
        for(int i=0;i<4;i++){
            reversedNames[j] = names[i];
            j = j-1;
        }
        
        for (int i = 0; i < reversedNames.length; i++) {
            System.out.print(reversedNames[i] + " ");
        }
        System.out.println("");
        
        Arrays.sort(numbers);
        System.out.println(Arrays.toString(numbers));
        
        DuplicateZeros();
    }

    private static void DuplicateZeros() {
        
    }
}